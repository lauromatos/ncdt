/*
 * NcdT -- directory tree printer with extended capabilities
 * (C) 1999-2001 by Pawel Wiecek <coven@vmh.net>
 * See Copying file for licence.
 *
 * Structures and prototypes
 */

#ifndef __NCDT_H__
#define __NCDT_H__

struct TREE {
 char *name;
 unsigned long len, time;
 int isdir, dirdown, brmin, brmax;
 struct TREE *brother, *son;
};

int endswith(char *, char *);
char *nicenum(unsigned long);
char *nicetime(unsigned long);
void help(void);
void t_scan(struct TREE *, char *);
int t_fix(struct TREE *, unsigned long *, int *, int *);
void t_print(struct TREE *, char *);
void t_free(struct TREE *);
void mp3info(char *, struct TREE *);

#endif
