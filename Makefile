# NcdT -- directory tree printer with extended capabilities
# (C) 1999-2001 by Pawel Wiecek <coven@vmh.net>
# See Copying file for licence.

CC=gcc
COPT=-O3 -Wall -pipe
LOPT=
OBJS=ncdt.o ncdt_mp3.o util.o
DEPS=ncdt.h

all: ncdt

ncdt: $(OBJS)
	$(CC) -o $@ $(OBJS) $(LOPT)
	strip $@

$(OBJS): $(DEPS)

.c.o:
	$(CC) -c $< $(COPT)

clean:
	-rm ncdt $(OBJS)

.PHONY: clean
