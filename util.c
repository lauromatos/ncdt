/*
 * NcdT -- directory tree printer with extended capabilities
 * (C) 1999-2001 by Pawel Wiecek <coven@vmh.net>
 * See Copying file for licence.
 * 
 * Utility functions
 */

#include <stdio.h>
#include <string.h>
#include "ncdt.h"

int endswith(char *s, char *e) {
 return !strcmp(s+strlen(s)-strlen(e),e);
}

char *nicenum(unsigned long n) {
 static char buf[1024];
 char tmp[5];
 
 if(n>999) {
  nicenum(n/1000);
  sprintf(tmp,",%03ld",n%1000);
  strcat(buf,tmp);
 } else {
  sprintf(buf,"%ld",n);
 }
 return buf;
}

char *nicetime(unsigned long n) {
 static char buf[1024];
 
 if(n>3600) sprintf(buf,"%d:%02d:%02d",(int)(n/3600),(int)((n/60)%60),
		    (int)(n%60));
       else sprintf(buf,"%d:%02d",(int)(n/60),(int)(n%60));
 return buf;
}

void help(void) {
 fprintf(stderr,"NcdT 2.0 -- simple format tree with extended capabilities\n"
	 "\nUsage:\n  ncdt [options] [directory [name]]\n\n"
	 "If directory is omitted, . (current directory) is assumed. Name "
	 "changes name\nused for root directory of the tree. If omitted the "
	 "value of directory is\nused.\n\nValid options:\n"
	 "  --dirs, -d\t  - print only directories, omit files\n"
	 "  --bitrate, -b\t  - print bitrate info for directories\n"
	 "  --prefix ...\t  - prefix listing with given text\n"
	 "  --help, -?\t  - display this information\n\n"
	 "Additional capabilities not found in standard tree are:\n"
	 " - size field for directories displays the summary size of "
	 "directory\n   subtree instead of the size of the special file\n"
	 " - sizes are displayed in a more readable format\n"
	 " - MP3 files are detected; additional info is displayed for "
	 "them\n\n");
 exit(0);
}
